<?php

require 'Slim/Slim.php';

$app = new Slim();

$app->get('/item', 'getItem');
$app->get('/item/:email', 'getItemByUserId');

$app->post('/item', 'addItem');
$app->delete('/item/:id', 'deleteItem');

$app->get('/shoppinglist', 'getShoppingList');
$app->post('/shoppinglist', 'addShoppingList');
$app->delete('/shoppinglist/:id', 'deleteShoppingList');

$app->get('/user', 'getUser');
$app->get('/user/:email', 'getUserByEmail');
$app->post('/user', 'addUser');
$app->put('/user/:id', 'updateUser');
$app->run();

function updateUser($id) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$user = json_decode($body);
	$sql = "UPDATE users SET email=:email, token=:token WHERE email=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("email", $user->email);
		$stmt->bindParam("token", $user->token);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
function getItemByUserId($email) {
    $sql = "SELECT * FROM items WHERE  user_id=:email";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $email);
        $stmt->execute();
        $items = $stmt->fetchAll();
        $db = null;
        // echo '{"items": ' . json_encode($items) . '}';
        echo json_encode($items);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getUserByEmail($email) {
    $sql = "SELECT id FROM users WHERE email=:email";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $email);
        $stmt->execute();
        $items = $stmt->fetchAll();
        $db = null;
        //echo '{"items": ' . json_encode($items) . '}';
        echo json_encode($items);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getUser() {
    $sql = "select * FROM users";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $items = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        // echo '{"items": ' . json_encode($items) . '}';
        echo json_encode($items);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function addUser() {
    $request = Slim::getInstance()->request();
    $item = json_decode($request->getBody());
    //var_dump($item);
    $sql = "INSERT INTO users  (email,token) VALUES (:email,:token)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $item->email);
        $stmt->bindParam("token", $item->token);
        $stmt->execute();
        $item->id = $db->lastInsertId();
        $db = null;
        echo json_encode($item);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function deleteShoppingList($id) {
    $sql = "DELETE FROM shoppinglist WHERE  id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getShoppingList() {
    $sql = "select * FROM shoppinglist";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $items = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        // echo '{"items": ' . json_encode($items) . '}';
        echo json_encode($items);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function addShoppingList() {

    $request = Slim::getInstance()->request();
    $item = json_decode($request->getBody());
    //var_dump($item);
    $sql = "INSERT INTO shoppinglist  (name) VALUES (:name)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("name", $item->name);


        $stmt->execute();
        $item->id = $db->lastInsertId();
        $db = null;
        echo json_encode($item);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function deleteItem($id) {
    $sql = "DELETE FROM items WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getItem() {
    $sql = "select * FROM items ORDER BY id asc";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $items = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        // echo '{"items": ' . json_encode($items) . '}';
        echo json_encode($items);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function addItem() {
    $request = Slim::getInstance()->request();
    $item = json_decode($request->getBody());
    //var_dump($item);
    $sql = "INSERT INTO items  (name,user_id) VALUES (:name,:user_id)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("name", $item->name);
        $stmt->bindParam("user_id", $item->user_id);

        $stmt->execute();
        $item->id = $db->lastInsertId();
        $db = null;
        echo json_encode($item);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getConnection() {
    $dbhost = "mysql.hostinger.es";
    $dbuser = "u495470798_cella";
    $dbpass = "sinnudos23";
    $dbname = "u495470798_cella";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

?>