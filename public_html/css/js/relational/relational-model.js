
var app = app || {};



app.User = Backbone.RelationalModel.extend({
    urlRoot: '/lista/api/users',
    //idAttribute: "id",
    relations: [{
            type: 'HasMany',
            key: 'nexo_user_shoplist',
            relatedModel: 'ShoppingList',
            reverseRelation: {
                key: 'nexo_shop_user',
                // includeInJSON: 'id'

            }
        }]
});

app.Item = Backbone.RelationalModel.extend({
    urlRoot: '/lista/api/item',
    idAttribute: "id"//attribute the server is using to uniquely identify each message 

});

app.ShoppingList = Backbone.RelationalModel.extend({
    urlRoot: '/lista/api/shoppinglist',
    idAttribute: "id", // by default itś  id, put it only to remember
    relations: [{
            type: Backbone.HasMany,
            key: 'items_id',
            relatedModel: 'app.Item',
            includeInJSON: Backbone.Model.prototype.idAttribute,
            reverseRelation: {
                key: 'shoppinglist_id',
                includeInJSON: 'id'
            }
        }]

});
House = Backbone.RelationalModel.extend({
    // The 'relations' property, on the House's prototype. Initialized separately for each
    // instance of House. Each relation must define (as a minimum) the 'type', 'key' and
    // 'relatedModel'. Options include 'includeInJSON', 'createModels' and 'reverseRelation'.
    relations: [
        {
            type: Backbone.HasMany, // Use the type, or the string 'HasOne' or 'HasMany'.
            key: 'occupants',
            relatedModel: 'Person',
            includeInJSON: Backbone.Model.prototype.idAttribute,
            collectionType: 'PersonCollection',
            reverseRelation: {
                key: 'livesIn'
            }
        }
    ]
});



app.ItemCollection = Backbone.Collection.extend(
        {
            model: app.Item,
            url: '/lista/api/item'

        });

app.ShoppingListCollection = Backbone.Collection.extend(
        {
            model: app.ShoppingList,
            url: '/lista/api/shoppinglist'
        });




