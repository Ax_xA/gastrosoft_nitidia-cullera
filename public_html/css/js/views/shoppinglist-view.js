
var app = app || {};

app.ListView = Backbone.View.extend({
    el: '#itemContainer',
    initialize: function () {
        this.name = this.$('#name');
        this.myShoppinglist = new app.shoppingList();
        this.myShoppinglist.fetch();
        this.renderAllItems();

        var ItemTrashView = new app.ItemTrashView();
        this.$el.append(ItemTrashView.render().el);

        this.listenTo(this.myShoppinglist, 'add', this.renderOneItem);
        this.listenTo(this.myShoppinglist, 'reset', this.renderAllItems);//TODO
    },
    events: {
        'click #add': 'addItem'
    },
    renderOneItem: function (item) {
        var itemView = new app.ItemView({model: item});
        this.$el.append(itemView.render().el);
    },
    renderAllItems: function () {
        this.myShoppinglist.each(function (item) {
            this.renderOneItem(item);
        }, this);
    },
    newData: function () {
        return {
            name: this.name.val().trim()
        };
    },
    addItem: function (event) {
        event.preventDefault();
        this.myShoppinglist.create(this.newData());//todo: validations
        this.name.val('');
    }


});