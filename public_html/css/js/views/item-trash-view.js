
var app = app || {};

app.ItemTrashView = Backbone.View.extend({
    tagName: 'div',
    className: 'trash',
    template: _.template($('#trashTemplate').html()),
    initialize: function () {

        $(this.el).droppable({// no funciona si creo un event drop( ??? )
            activeClass: 'drop-state-hover',
            hoverClass: 'ui-state-active',
            drop: function (event, ui) {
                var model = $(ui.draggable).data("backbone-view").model;
                model.destroy();
                $(ui.draggable).remove();
            }
        });

    },
    render: function () {
        this.$el.html(this.template());
        return this;

    }
});