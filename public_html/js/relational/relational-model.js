
var app = app || {};

app.User000 = Backbone.RelationalModel.extend({
    urlRoot: '/lista/api/user',
    idAttribute: "id", // by default itś  id
    defaults: {
        email: null
    },
    relations: [
        {// Create a (recursive) one-to-one relationship
            type: Backbone.HasOne,
            key: 'shoppinglist_id',
            relatedModel: 'app.ShoppingList',
            includeInJSON: Backbone.Model.prototype.idAttribute,
        }
    ],
    initialize: function () {
        console.log('creando a new User');
    }
    /*
     * 
     validate: function(attrs){
     if(attrs.year< 2000){
     return 'Year must be after 2000';
     }
     if(!attrs.name){
     return 'A name must be provided';
     }
     }
     * 
     * 
     */

});
app.User = Backbone.RelationalModel.extend({
    urlRoot: '/lista/api/user',
    idAttribute: "id", // by default itś  id, put it only to remember
    initialize: function () {
        console.log('Creando User');
    }

});

app.Item = Backbone.RelationalModel.extend({
    urlRoot: '/lista/api/item',
    //idAttribute: "id", //attribute the server is using to uniquely identify each message 
    relations: [{
            type: Backbone.HasOne,
            key: 'user_id',
            relatedModel: 'app.User',
            includeInJSON: 'id'
        }],
    initialize: function () {
        console.log('Creando item');
    }
});
app.ShoppingList = Backbone.Collection.extend(
        {
            model: app.Item,
            url: '/lista/api/item'

        });

app.ShoppingListByUserColl = Backbone.Collection.extend({
      model: app.Item,            
    url: function () {
        return '/lista/api/item/' + this.user_id;
    },
    initialize: function (opts) {
        this.user_id = opts.user_id;
    }
});


app.UserByEmail= Backbone.RelationalModel.extend({
model: app.User,
url: function () {
        return '/lista/api/user/' + this.genre;
    },
    initialize: function (opts) {
        this.genre = opts.genre;
                        console.log('Creating User by id');

    }
});
app.UserCollection = Backbone.Collection.extend(
        {
            model: app.User,
            url: '/lista/api/user',
            initialize: function () {
                console.log('Creating a new User collection');
            }
        });



