
var app = app || {};

app.UserShoppingListView = Backbone.View.extend({
    // el: '#itemContainer',
    initialize: function () {
        this.name = this.$('#name');
        this.user_id = this.model.get('id');
        console.log('User id in UserShoppingListView : ' + this.model.get('id'));

        this.myShoppinglist = new app.ShoppingListByUserColl({user_id: this.user_id});
        this.myShoppinglist.fetch({
            success: function () {
                console.log('fetching shoppinglist');
            }, error: function () {
                console.log('Something went wrong');
            }
        });

        var ItemTrashView = new app.ItemTrashView();
        this.$el.append(ItemTrashView.render().el);

        this.listenTo(this.myShoppinglist, 'add', this.renderOneItem);
        this.listenTo(this.myShoppinglist, 'reset', this.renderAllItems);

    }, // end initialize

    events: {
        'click #add': 'addItem'
    },
    renderOneItem: function (item) {
        console.log('renderOneItem');
        var itemView = new app.ItemView({model: item});
        this.$el.append(itemView.render().el);
    },
    renderAllItems: function () {
        this.myShoppinglist.each(function (item) {
            this.renderOneItem(item);
        }, this);
    },
    newData: function () {
        return {
            name: this.name.val().trim(),
            user_id: this.user_id
        };
    },
    addItem: function (event) {
        event.preventDefault();
        this.myShoppinglist.create(this.newData());//todo: validations
        this.name.val('');
    }
});
