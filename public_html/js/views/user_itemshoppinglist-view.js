
var app = app || {};

app.UserItemShoppingListView = Backbone.View.extend({
    tagName: 'div',
    className: 'test',
    template: _.template($('#itemTemplate').html()),
    initialize: function () {

        $(this.el).data("backbone-view", this);//Asi se pasa el modelo al droppable

        $(this.el).draggable({
            cursorAt: {
                top: 0,
                left: 0
            },
            helper: 'clone',
            scroll: false,
            tolerance: 'pointer'
        });

        },
    events: {
        'click .delete': 'deleteItem'// todo :remove, not used, just for example
    },
    deleteItem: function () {// todo:remove, not used
        this.model.destroy();
        this.remove();
    },
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;

    }
});