
(function ($) {

    $.forum = {}
    var myUser = new app.User();
    var myUserColl = new app.UserCollection();

    // Router ///////////////////////////////////////////////////////////////

    $.forum.Router = Backbone.Router.extend({
        routes: {
            "lista/": "show_thread_list"
        },
        show_thread_list: function () {


            this.listenToOnce(myUser, 'add', this.userChange);
            this.listenToOnce(myUserColl, 'add', this.collChange);

            myUserColl.fetch({
                success: function () {
                    console.log('success myUserColl.fetch');

                }, error: function () {
                    console.log('Something went wrong-myUserColl.fetch');
                }
            });

        },
        userChange: function () {

            console.log('userChange:' + myUser.get('id'));
            var thread_list_view = new app.UserShoppingListView({el: $('#itemContainer'),
                model: myUser});
        },
        collChange: function () {
            console.log('collChange');
            myUser = myUserColl.findWhere({email: 'lolo'});

            if (myUser == void 0) {
                console.log('collChange: not  exists: ');

                myUser = new app.User({email: 'new-one',
                    token: 'new'});
                
            this.listenToOnce(myUser, 'add', this.userChange);

                myUser.save(myUser.attributes,
                        {
                            success: function (model, response, options) {
                                console.log('Model saved');
                                console.log('Id: ' + myUser.get('id'));
                              
                            },
                            error: function (model, xhr, options) {
                                console.log('Failed to save model');
                            }
                        });

            } else {
                console.log('collChange_User: already  exists: ' + myUser.get('id'));
                var thread_list_view = new app.UserShoppingListView({el: $('#itemContainer'),
                    model: myUser});
            }


        }

    });
    // App /////////////////////////////////////////////////////////////////

    $.forum.app = null;
    $.forum.bootstrap = function () {
        $.forum.app = new $.forum.Router();
        Backbone.history.start({pushState: true});
    };
})(jQuery);